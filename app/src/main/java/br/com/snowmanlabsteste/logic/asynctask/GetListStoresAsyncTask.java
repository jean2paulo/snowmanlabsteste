package br.com.snowmanlabsteste.logic.asynctask;

import android.os.AsyncTask;

import java.util.List;

import br.com.snowmanlabsteste.MyApplication;
import br.com.snowmanlabsteste.model.Store;
import br.com.snowmanlabsteste.tools.AsyncTaskResult;
import br.com.snowmanlabsteste.ui.activity.MainActivity;
import retrofit.RetrofitError;

/**
 * Created by jean.almeida on 12/05/2015.
 */
public class GetListStoresAsyncTask extends AsyncTask<Double, Void, AsyncTaskResult<List<Store>>> {

    private MainActivity mainActivity;

    public GetListStoresAsyncTask(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    protected AsyncTaskResult<List<Store>> doInBackground(Double... params) {
        try {
            return new AsyncTaskResult<List<Store>>(MyApplication.ServerConnection.getListStores());
        } catch (RetrofitError error) {
            int x= 0;
            return new AsyncTaskResult<List<Store>>(error);
        }
    }

    @Override
    protected void onPostExecute(AsyncTaskResult<List<Store>> result) {
        if (result.getResult() != null) {
            mainActivity.setListStore(result.getResult());
        } else {
            //TODO Tratar erro de conexão
        }
    }

}
