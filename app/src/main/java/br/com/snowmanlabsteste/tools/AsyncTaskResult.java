package br.com.snowmanlabsteste.tools;

import retrofit.RetrofitError;

public class AsyncTaskResult<T> {
    private T result;
    private RetrofitError error;

    public T getResult() {
        return result;
    }

    public RetrofitError getError() {
        return error;
    }

    public AsyncTaskResult(T result) {
        super();
        this.result = result;
    }

    public AsyncTaskResult(RetrofitError error) {
        super();
        this.error = error;
    }
}