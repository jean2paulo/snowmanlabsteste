package br.com.snowmanlabsteste.ui.fragment;

import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import br.com.snowmanlabsteste.R;
import br.com.snowmanlabsteste.model.Store;
import br.com.snowmanlabsteste.ui.activity.MainActivity;
import br.com.snowmanlabsteste.ui.adapter.PlaceInfoAdapter;

public class MapFragment extends Fragment {

    public GoogleMap googleMap;
    private MapView mMapView;
    private static CameraPosition cp;
    public static HashMap<Marker, Store> markerMap = new HashMap<Marker, Store>();
    private ProgressBar prb_loading;

    public static MapFragment newInstance() {
        MapFragment fragment = new MapFragment();
        return fragment;
    }

    public MapFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MapsInitializer.initialize(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_map, null);

        prb_loading = (ProgressBar) rootView.findViewById(R.id.prb_loading);
        mMapView = (MapView) rootView.findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);

        googleMap = mMapView.getMap();
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.setMyLocationEnabled(true);
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //addMarkers();

        googleMap.setInfoWindowAdapter(new PlaceInfoAdapter(getActivity()));
        googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {

            @Override
            public void onInfoWindowClick(Marker arg0) {
                Store store = markerMap.get(arg0);
                ((MainActivity)getActivity()).showProducts(store);
            }
        });

        return rootView;
    }

    public void setListStores(List<Store> stores){
        prb_loading.setVisibility(View.GONE);
        addMarkers(stores);
    }

    public void addMarkers(List<Store> stores) {
        if (stores.size() > 0) {
            for (Store store : stores) {
                // Create user marker with custom icon and other options
                MarkerOptions markerOption = getMarkerForObject(store);
                Marker currentMarker = googleMap.addMarker(markerOption);
                markerMap.put(currentMarker, store);
            }
        }
    }

    private MarkerOptions getMarkerForObject(Store store) {
        LatLng location = new LatLng(store.getLatitude(), store.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions().position(location);
        //markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher));
        return markerOptions;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(MainActivity.MAP_FRAGMENT);
    }

    @Override
    public void onPause() {
        mMapView.onPause();
        super.onPause();

        cp = googleMap.getCameraPosition();
    }

    @Override
    public void onResume() {
        super.onResume();

        mMapView.onResume();
        if (cp != null) {
            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cp));
            cp = null;
        }
    }

    @Override
    public void onDestroy() {
        mMapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        mMapView.onLowMemory();
        super.onLowMemory();
    }
}