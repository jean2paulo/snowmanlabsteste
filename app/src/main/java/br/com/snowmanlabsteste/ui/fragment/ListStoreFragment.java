package br.com.snowmanlabsteste.ui.fragment;

import java.util.List;

import br.com.snowmanlabsteste.R;
import br.com.snowmanlabsteste.model.Store;
import br.com.snowmanlabsteste.ui.activity.MainActivity;
import br.com.snowmanlabsteste.ui.adapter.ItensAdapter;
import br.com.snowmanlabsteste.ui.adapter.StoreAdapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

public class ListStoreFragment extends Fragment {

    private ListView lst_stores;
    private ProgressBar prb_loading;

    public static ListStoreFragment newInstance() {
        ListStoreFragment fragment = new ListStoreFragment();
        return fragment;
    }

    public ListStoreFragment() {}
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list_stores, container, false);
        lst_stores = (ListView) rootView.findViewById(R.id.lst_stores);
        prb_loading = (ProgressBar) rootView.findViewById(R.id.prb_loading);
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(MainActivity.LIST_STORE_FRAGMENT);
    }

    public void setListStores(List<Store> stores){
        prb_loading.setVisibility(View.GONE);

        final StoreAdapter storeAdapter = new StoreAdapter(getActivity(), stores);
        lst_stores.setAdapter(storeAdapter);

        lst_stores.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Store store = storeAdapter.getItem(position);
                ((MainActivity) getActivity()).showProducts(store);
            }
        });
    }


}
