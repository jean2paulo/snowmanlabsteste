package br.com.snowmanlabsteste.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by JEAN PAULO on 5/12/2015.
 */
public class SlidingPagerAdapter extends FragmentPagerAdapter {

    private String[] fragmentNames;
    private Fragment[] fragments;

    public SlidingPagerAdapter(FragmentManager fm, Fragment[] fragments, String[] fragmentNames) {
        super(fm);
        this.fragments = fragments;
        this.fragmentNames = fragmentNames;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentNames[position];
    }

    @Override
    public int getCount() {
        return fragments.length;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }
}