package br.com.snowmanlabsteste.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;

import br.com.snowmanlabsteste.R;
import br.com.snowmanlabsteste.model.Store;
import br.com.snowmanlabsteste.tools.DownloadImageTask;
import br.com.snowmanlabsteste.ui.fragment.MapFragment;

public class PlaceInfoAdapter implements InfoWindowAdapter {

    LayoutInflater inflater;
    Context context;

    public PlaceInfoAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(this.context);
    }

    // Use default InfoWindow frame
    @Override
    public View getInfoWindow(Marker arg0) {
        return null;
    }

    // Defines the contents of the InfoWindow
    @Override
    public View getInfoContents(Marker arg0) {
        
        Store store = MapFragment.markerMap.get(arg0);
        
        // Getting view from the layout file info_window_layout
        View convertView = inflater.inflate(R.layout.layout_place_info, null);
        TextView txt_company = (TextView) convertView.findViewById(R.id.txt_company);
        TextView txt_telephone = (TextView) convertView.findViewById(R.id.txt_telephone);
        TextView txt_email = (TextView) convertView.findViewById(R.id.txt_email);

        txt_company.setText(store.getCompany());
        txt_telephone.setText(store.getPhone());
        txt_email.setText(store.getEmail());

        return convertView;

    }

}
