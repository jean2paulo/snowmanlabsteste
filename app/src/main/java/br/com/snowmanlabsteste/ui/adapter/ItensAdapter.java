package br.com.snowmanlabsteste.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.snowmanlabsteste.R;
import br.com.snowmanlabsteste.model.Product;
import br.com.snowmanlabsteste.model.Store;
import br.com.snowmanlabsteste.tools.DownloadImageTask;

/**
 * Created by JEAN PAULO on 5/12/2015.
 */
public class ItensAdapter  extends BaseAdapter {

    private List<Product> productList;

    private LayoutInflater inflater;
    private Context context;

    public ItensAdapter(Context context, List<Product> productList) {
        this.context = context;
        this.productList = productList;
        inflater = LayoutInflater.from(this.context); // only context can also
    }

    @Override
    public int getCount() {
        return productList.size();
    }

    @Override
    public Product getItem(int position) {
        return productList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        MyViewHolder mViewHolder = new MyViewHolder();
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.layout_product, null);

            mViewHolder.txt_id = (TextView) convertView.findViewById(R.id.txt_id);
            mViewHolder.txt_name = (TextView) convertView.findViewById(R.id.txt_name);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

        final Product product = productList.get(position);

        mViewHolder.txt_id.setText(product.getId()+"");
        mViewHolder.txt_name.setText(product.getName());
        return convertView;
    } // or you can try better way

    private class MyViewHolder {
        TextView txt_id, txt_name;
    }
}