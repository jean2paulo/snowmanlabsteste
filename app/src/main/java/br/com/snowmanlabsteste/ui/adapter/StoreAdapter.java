package br.com.snowmanlabsteste.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.snowmanlabsteste.R;
import br.com.snowmanlabsteste.model.Store;
import br.com.snowmanlabsteste.tools.DownloadImageTask;

/**
 * Created by jean.almeida on 12/05/2015.
 */
public class StoreAdapter extends BaseAdapter {

    private List<Store> storeList;

    private LayoutInflater inflater;
    private Context context;

    public StoreAdapter(Context context, List<Store> storeList) {
        this.context = context;
        this.storeList = storeList;
        inflater = LayoutInflater.from(this.context); // only context can also
    }

    @Override
    public int getCount() {
        return storeList.size();
    }

    @Override
    public Store getItem(int position) {
        return storeList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        MyViewHolder mViewHolder = new MyViewHolder();
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.layout_store, null);

            mViewHolder.txt_company = (TextView) convertView.findViewById(R.id.txt_company);
            mViewHolder.txt_email = (TextView) convertView.findViewById(R.id.txt_email);
            mViewHolder.txt_telephone = (TextView) convertView.findViewById(R.id.txt_telephone);
            mViewHolder.txt_price = (TextView) convertView.findViewById(R.id.txt_price);
            mViewHolder.img_picture = (ImageView) convertView.findViewById(R.id.img_picture);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

        final Store store = storeList.get(position);

        mViewHolder.txt_company.setText(store.getCompany());
        mViewHolder.txt_email.setText(store.getEmail());
        mViewHolder.txt_telephone.setText(store.getPhone());
        mViewHolder.txt_price.setText(store.getPrice());
        new DownloadImageTask(mViewHolder.img_picture).execute(store.getPicture());

        return convertView;
    } // or you can try better way

    private class MyViewHolder {
        TextView txt_company, txt_email, txt_telephone, txt_price;
        ImageView img_picture;
    }
}
