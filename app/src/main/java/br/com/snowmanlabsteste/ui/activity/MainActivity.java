package br.com.snowmanlabsteste.ui.activity;

import android.app.AlertDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;
import java.util.Map;

import br.com.snowmanlabsteste.R;
import br.com.snowmanlabsteste.logic.asynctask.GetListStoresAsyncTask;
import br.com.snowmanlabsteste.model.Store;
import br.com.snowmanlabsteste.tools.SlidingTabLayout;
import br.com.snowmanlabsteste.ui.adapter.ItensAdapter;
import br.com.snowmanlabsteste.ui.adapter.SlidingPagerAdapter;
import br.com.snowmanlabsteste.ui.fragment.ListStoreFragment;
import br.com.snowmanlabsteste.ui.fragment.MapFragment;

public class MainActivity extends ActionBarActivity {

    public static final int MAP_FRAGMENT = 0;
    public static final int LIST_STORE_FRAGMENT = 1;
    private boolean isMapFragmentOk = false, isListFragmentOk = false;

    private ListStoreFragment listStoreFragment;
    private MapFragment mapFragment;
    private List<Store> stores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fragmentManager = getSupportFragmentManager();
        mapFragment = MapFragment.newInstance();
        listStoreFragment = ListStoreFragment.newInstance();

        String[] fragmentNames = new String[] {getString(R.string.map), getString(R.string.list)};

        SlidingPagerAdapter adapter = new SlidingPagerAdapter(getSupportFragmentManager(), new Fragment[] {mapFragment, listStoreFragment}, fragmentNames);

        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

        SlidingTabLayout tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width
        tabs.setViewPager(pager);
        tabs.setSelectedIndicatorColors(getResources().getColor(android.R.color.white));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void setListStore(List<Store> stores){
        this.stores = stores;

        mapFragment.setListStores(stores);
        listStoreFragment.setListStores(stores);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onSectionAttached(int fragment) {
        if(fragment == MAP_FRAGMENT) isMapFragmentOk = true;
        if(fragment == LIST_STORE_FRAGMENT) isListFragmentOk = true;

        if(isMapFragmentOk && isListFragmentOk) {
            GetListStoresAsyncTask getListStoresAsyncTask = new GetListStoresAsyncTask(this);
            getListStoresAsyncTask.execute();
        }
    }
    public void showProducts(Store store){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(String.format(getString(R.string.products_of), store.getCompany()));
        builder.setAdapter(new ItensAdapter(this, store.getProducts()), null);
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
