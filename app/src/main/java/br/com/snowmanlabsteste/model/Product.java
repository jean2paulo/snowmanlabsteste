package br.com.snowmanlabsteste.model;

/**
 * Created by jean.almeida on 12/05/2015.
 */
public class Product {

    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
