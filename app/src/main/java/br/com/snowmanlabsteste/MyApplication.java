package br.com.snowmanlabsteste;

import android.app.Application;

import br.com.snowmanlabsteste.webservices.WebService;
import retrofit.RestAdapter;
import retrofit.android.AndroidLog;


/**
 * Created by jean.almeida on 12/05/2015.
 */
public class MyApplication extends Application {

    private String URL = "http://www.json-generator.com/api/json/get";
    public static WebService ServerConnection;

    @Override
    public void onCreate() {
        super.onCreate();

        //Configura��o do servidor
        setServerConnection();
    }

    public void setServerConnection() {
        RestAdapter.Builder builder = new RestAdapter.Builder().setEndpoint(URL);
        //builder.setLogLevel(RestAdapter.LogLevel.FULL);
        //builder.setLog(new AndroidLog("Consumo do Servi�o --> "));

        ServerConnection = builder.build().create(WebService.class);
    }
}
