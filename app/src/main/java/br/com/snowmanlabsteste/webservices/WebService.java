package br.com.snowmanlabsteste.webservices;

import java.util.List;

import br.com.snowmanlabsteste.model.Store;
import retrofit.http.GET;

/**
 * Created by jean.almeida on 12/05/2015.
 */
public interface WebService {

    @GET("/bKUwIPambm?indent=3")
    List<Store> getListStores();
}
